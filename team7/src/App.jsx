import React from "react";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import Home from "./module/home/Home";
import About from "./module/about/about";
import Service from "./module/service/Service";
import Contact from "./module/contact/contact";
import Pricing from "./module/pages/Pricing";
import RootLayout from "./module/layout/RootLayout";
import PagesLayout from "./module/layout/PagesLayout";
import Feature from "./module/pages/Feature";
import FreeQuote from "./module/pages/FreeQuote";
import OurTeamPage from "./module/pages/OurTeamPage";
import Testimonial from "./module/components/Testimonail";
import Error from "./module/pages/Error";
// import AOS from 'aos';
// import 'aos/dist/aos.css';
// AOS.init();

const App = () => {
  return (
    <Router>
      <Routes>
        <Route element={<RootLayout />}>
          <Route index element={<Home />} />
          <Route path="about" element={<About />} />
          <Route path="service" element={<Service />} />
          <Route element={<PagesLayout />}>
            <Route path="pricing" element={<Pricing />} />
            <Route path="feature" element={<Feature />} />
            <Route path="freequote" element={<FreeQuote />} />
            <Route path="ourteam" element={<OurTeamPage />} />
            <Route path="testimonail" element={<Testimonial />} />
            <Route path="error" element={<Error />} />
          </Route>
          <Route path="contact" element={<Contact />} />
        </Route>
      </Routes>
    </Router>
  );
};

export default App;
