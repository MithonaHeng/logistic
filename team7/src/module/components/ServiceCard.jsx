import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';

const ServiceCard = ({ serviceInfo }) => {
    let time = '500';
    return (
        <div className='container py-5'>
            <div className='row'>
                <div className='col-md-12 text-center pb-3'>
                    <h6 className='ourteamcolor1 text-uppercase'><b>Our Services</b></h6>
                    <h1 className='ourteamcolor2 mb-5'><b>Explore Our Services</b></h1>
                </div>
                {serviceInfo.map((info, index) => (
                    <div key={index} className='col-md-4 mb-4 mx-auto'>

                        {/*  Cards */}
                        <div className="card" data-aos="fade-up" data-aos-duration={`${time * info.id}`} style={{ width: '410px', height: '450px', borderRadius: '0', borderWidth: '0'}}>
                            <div className="card-body">
                                <div className='overflow-hidden'>
                                    <img src={info.image} className="card-img-top card-img-zoom" alt={info.fullName} />
                                </div>
                                <h5 className="card-title pt-3"><b>{info.fullName}</b></h5>
                                <p className="card-text py-2">{info.designation}</p>
                                <div className="rightsidebtn">
                                    <div className='position-relative overflow-hidden readmoreCon'>
                                        <i className="rightside-menu fa-solid fa-arrow-right arrhover"style={{zIndex:'99',position:'relative'}}></i>
                                        <p className='readmoresss readhover'>Read More</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                ))}
            </div>
        </div>
    );
}

export default ServiceCard;