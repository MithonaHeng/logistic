    import React from 'react';

    const ContactForm = () => {
    return (
        <div style={{ width: '630px' }}>
            <div className="container-fluid px-5 my-5" style={{ backgroundColor: 'rgb(248,242,240)' }}>
                <div className="row">
                    <div className="card-body p-0">
                        <div className="row g-0">
                            <div className="p-4">
                            <form id="contactForm">
                                <div className="row">
                                <div className="col-md-6 mb-3">
                                    <div className="form-floating">
                                    <input className="form-control" id="name" type="text" placeholder="Name" />
                                    <label htmlFor="name">Name</label>
                                    </div>
                                </div>

                                <div className="col-md-6 mb-3">
                                    <div className="form-floating">
                                    <input className="form-control" id="emailAddress" type="email" placeholder="Email Address" />
                                    <label htmlFor="emailAddress">Email</label>
                                    </div>
                                </div>
                                </div>

                                <div className="form-floating mb-3">
                                    <input className="form-control" id="subject" type="subject" placeholder="Subject" />
                                    <label htmlFor="emailAddress">Subject</label>
                                    </div>

                                <div className="form-floating mb-3">
                                <textarea className="form-control" id="message" placeholder="Message" style={{ height: '6rem' }}></textarea>
                                <label htmlFor="message">Message</label>
                                </div>

                                <div className="d-grid">
                                <button className="btnSubmit" id="submitButton" type="submit">
                                    <b>Sent Message</b>
                                </button>
                                </div>
                            </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
    };

    export default ContactForm;
