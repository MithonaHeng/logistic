import React, { useRef, useState } from "react";
import { Swiper, SwiperSlide } from "swiper/react";
import { Pagination } from "swiper/modules";
import "swiper/css";
import "swiper/css/free-mode";
import "swiper/css/pagination";
import t1 from "../../assets/image/testimonial-1.jpg";
import t2 from "../../assets/image/testimonial-2.jpg";
import t3 from "../../assets/image/testimonial-3.jpg";
import t4 from "../../assets/image/testimonial-4.jpg";

const owl = () => {
  return (<>
    <div className="container py-5">
      <div className="container py-5">
        <div className="text-center">
          <h5 className=" text-uppercase fw-bold text-info">Testimonail</h5>
          <h1>Our Clients Say!</h1>
        </div>
      </div>
      <Swiper
        slidesPerView={3}
        spaceBetween={20}
        pagination={{
          clickable: true,
        }}
        modules={[Pagination]}
        className="mySwiper"
      >
        <div className="container d-flex p-5 bg-black">
          <SwiperSlide>
            <div className="p-4 my-4 shadow">
              <i className="fa fa-quote-right fa-3x text-light position-absolute top-0 end-0 mt-n3 me-4"></i>
              <div className="d-flex align-items-end mb-4">
                <img src={t1} alt=""  style={{width:'80px',height:"80px"}} className=" img-fluid"/>
                <div className=" ms-4">
                  <h5>Client Name</h5>
                  <p className=" m-0">Professional</p>
                </div>
              </div>
              <p className="mb-0">Tempor erat elitr rebum at clita. Diam dolor diam ipsum sit diam amet diam et eos.
              ipsum sit diam amet diam et eos.
               </p>
            </div>
          </SwiperSlide>
          <SwiperSlide>
            <div className="p-4 my-4 shadow">
              <i className="fa fa-quote-right fa-3x text-light position-absolute top-0 end-0 mt-n3 me-4"></i>
              <div className="d-flex align-items-end mb-4">
                <img src={t2} alt=""  style={{width:'80px',height:"80px"}} className=" img-fluid"/>
                <div className=" ms-4">
                  <h5>Client Name</h5>
                  <p className=" m-0">Professional</p>
                </div>
              </div>
              <p className="mb-0">Tempor erat elitr rebum at clita. Diam dolor diam ipsum sit diam amet diam et eos.
              ipsum sit diam amet diam et eos.
               </p>
            </div>
          </SwiperSlide>
          <SwiperSlide>
            <div className="p-4 my-4 shadow">
              <i className="fa fa-quote-right fa-3x text-light position-absolute top-0 end-0 mt-n3 me-4"></i>
              <div className="d-flex align-items-end mb-4">
                <img src={t3} alt=""  style={{width:'80px',height:"80px"}} className=" img-fluid"/>
                <div className=" ms-4">
                  <h5>Client Name</h5>
                  <p className=" m-0">Professional</p>
                </div>
              </div>
              <p className="mb-0">Tempor erat elitr rebum at clita. Diam dolor diam ipsum sit diam amet diam et eos.
              ipsum sit diam amet diam et eos.
               </p>
            </div>
          </SwiperSlide>
          <SwiperSlide>
            <div className="p-4 my-4 shadow">
              <i className="fa fa-quote-right fa-3x text-light position-absolute top-0 end-0 mt-n3 me-4"></i>
              <div className="d-flex align-items-end mb-4">
                <img src={t4} alt=""  style={{width:'80px',height:"80px"}} className=" img-fluid"/>
                <div className=" ms-4">
                  <h5>Client Name</h5>
                  <p className=" m-0">Professional</p>
                </div>
              </div>
              <p className="mb-0">Tempor erat elitr rebum at clita. Diam dolor diam ipsum sit diam amet diam et eos.
              ipsum sit diam amet diam et eos.
               </p>
            </div>
          </SwiperSlide>
          <SwiperSlide>
            <div className="p-4 my-4 shadow">
              <i className="fa fa-quote-right fa-3x text-light position-absolute top-0 end-0 mt-n3 me-4"></i>
              <div className="d-flex align-items-end mb-4">
                <img src={t1} alt=""  style={{width:'80px',height:"80px"}} className=" img-fluid"/>
                <div className=" ms-4">
                  <h5>Client Name</h5>
                  <p className=" m-0">Professional</p>
                </div>
              </div>
              <p className="mb-0">Tempor erat elitr rebum at clita. Diam dolor diam ipsum sit diam amet diam et eos.
              ipsum sit diam amet diam et eos.
               </p>
            </div>
          </SwiperSlide>
          <SwiperSlide>
            <div className="p-4 my-5 shadow">
              <i className="fa fa-quote-right fa-3x text-light position-absolute top-0 end-0 mt-n3 me-4"></i>
              <div className="d-flex align-items-end mb-4">
                <img src={t2} alt=""  style={{width:'80px',height:"80px"}} className=" img-fluid"/>
                <div className=" ms-4">
                  <h5>Client Name</h5>
                  <p className=" m-0">Professional</p>
                </div>
              </div>
              <p className="mb-0">Tempor erat elitr rebum at clita. Diam dolor diam ipsum sit diam amet diam et eos.
              ipsum sit diam amet diam et eos.
               </p>
            </div>
          </SwiperSlide>
        </div>
      </Swiper>
    </div>
    </>);
};

export default owl;
