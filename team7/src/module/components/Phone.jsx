import React from 'react'
import '../../assets/css/phone.css'
function Phone() {
  return (
<article className="phone d-flex align-content-center my-5">
        <i className="fa-solid fa-headphones"></i>
        <div className="number ms-4">
                <h6 className=' text-dark'>Call for any query!</h6>
                <h3>+012 345 6789</h3>
        </div>
</article> )
}

export default Phone