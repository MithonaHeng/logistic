import React from 'react'
import background from '../../assets/image/carousel-1.jpg'
import { useLocation, useParams } from 'react-router-dom'

function SmallBanner() {
  const location = useLocation()
  const handleText = [
    {title: "Pricing", value: "/pricing"},
    {title: "Features", value: "/feature"},
    {title: "Free Quote", value: "/freequote"},
    {title: "Not Found", value: "/error"},
    {title: "About Us", value: "/about"},
    {title: "Our Team", value: "/ourteam"},
    {title: "Service", value: "/service"},
    {title: "Contact Us", value: "/contact"},
    {title: "Testimonail", value: "/testimonail"},
  ]

  return (
    <>
    <div className='banner' style={{ backgroundImage: `url(${background})`, 
    minHeight: '310px',
    width: '100%',
    objectFit:'cover',
    backgroundRepeat: 'no-repeat',
    backgroundPosition: 'center',
    backgroundSize: 'cover',
    position: 'relative'
    }}>
   <div style={{
  width: '100%',
  height: '100%',
  backgroundColor: 'rgba(0, 0, 0, 0.4)',
  position: 'absolute'
}}>
</div>
    </div> 
        
          {
            handleText.filter(item=>location.pathname===item.value).map((items,index)=> {
              return(
                <div style={{position: "absolute", top: "15rem",marginLeft:"8rem",}} key={index} >
                <h1 className='display-4 text-white' data-aos="fade-down">{items.title}</h1>
                <div className="text-white">
                <p className='text-white'><a className='text-white' href="#">Home</a> / <a className='text-white' href="#">Pages</a> /
                 {items.title}</p>
                </div>
                </div>
              )
            })
          }
          
        
    </>
  );
}

export default SmallBanner