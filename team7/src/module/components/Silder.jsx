import React from 'react'
import 'bootstrap/dist/css/bootstrap.min.css';
import Img1 from "../../assets/image/carousel-1.jpg"
import Img2 from "../../assets/image/carousel-2.jpg"


const Silder = () => {
  return (
    <div id="carouselExample" className="carousel slide">
  <div className="carousel-inner">
    <div className="carousel-item active">
      <img src={Img1} className="d-block w-100" alt="..."/>
      <div className=' position-absolute top-0   start-0 d-flex w-100 h-100  align-items-center' style={{background:'rgba(6, 3, 21, .5)'}}>
       <div className="container mt-4">
        <div className="row justify-content-start">
          <div className="col-lg-8">
            <h5 className='text-uppercase  text-light fs-5 fw-bolder'>transport & logistics soukution</h5>
             <h1 className='text-light mb-4' style={{fontSize:'3.5rem'}}>
              # 1 Place for Your <span className=' text-danger'>Logistice</span> <br />
              Solution
             </h1>
             <p className='text-light fs-5 fw-bold mb-5'>Vero elitr justo clita lorem. Ipsum dolor at sed stet sit diam no. Kasd rebum ipsum et diam justo clita et kasd rebum sea elitr.</p>
             <div className=''>
             <button type="button" className="btn btn-danger px-5 py-3 fw-bold fs-6" style={{ borderRadius: '0'}}>Read More</button>
             <button type="button" className="btn btn-info px-5 py-3 fw-bold fs-6 mx-4 text-light" style={{ borderRadius: '0'}}>Free Quote</button>
             </div>
          </div>
        </div>
      </div>
      </div>
    </div>
    <div className="carousel-item position-relative">
      <img src={Img2} className="d-block w-100" alt="..."/>
      <div className=' position-absolute top-0   start-0 d-flex w-100 h-100  align-items-center' style={{background:'rgba(6, 3, 21, .5)'}}>
       <div className="container mt-4">
        <div className="row justify-content-start">
          <div className="col-lg-8">
            <h5 className='text-uppercase  text-light fs-5 fw-bolder'>transport & logistics soukution</h5>
             <h1 className='text-light mb-4' style={{fontSize:'3.5rem'}}>
              # 1 Place for Your <span className=' text-danger'>Logistice</span> <br />
              Solution
             </h1>
             <p className='text-light fs-5 fw-bold mb-5'>Vero elitr justo clita lorem. Ipsum dolor at sed stet sit diam no. Kasd rebum ipsum et diam justo clita et kasd rebum sea elitr.</p>
             <div className=''>
             <button type="button" className="btn btn-danger px-5 py-3 fw-bold fs-6" style={{ borderRadius: '0'}}>Read More</button>
             <button type="button" className="btn btn-info px-5 py-3 fw-bold fs-6 mx-4 text-light" style={{ borderRadius: '0'}}>Free Quote</button>
             </div>
          </div>
        </div>
      </div>
      </div>

    </div>
  </div>
  <button className="carousel-control-prev " type="button" data-bs-target="#carouselExample" data-bs-slide="prev">
  <span className="carousel-control-prev-icon p-4 bg-black" aria-hidden="true" style={{ borderRadius: '50%', border:'3px solid white'}}></span>
    <span className="visually-hidden">Previous</span>
  </button>
  <button className="carousel-control-next" type="button" data-bs-target="#carouselExample" data-bs-slide="next">
    <span className="carousel-control-next-icon p-4 bg-black" aria-hidden="true" style={{ borderRadius: '50%', border:'3px solid white'}}></span>
    <span className="visually-hidden">Next</span>
  </button>
</div>
  )
}

export default Silder