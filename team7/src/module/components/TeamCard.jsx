import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';

const TeamCard = ({ teamMembers }) => {
    let time = '500';
    return (
        <div className='container py-5'>
            <div className='row'>
                <div className='col-md-12 text-center pb-3'>
                    <h6 className='ourteamcolor1 text-uppercase'><b>Our Team</b></h6>
                    <h1 className='ourteamcolor2 mb-5'><b>Expert Team Members</b></h1>
                </div>
                {teamMembers.map((member, index) => (
                    <div key={index} className='col-md-3 mb-4 mx-auto'>

                        {/*  Cards */}
                        <div className="card" data-aos="fade-up" data-aos-duration={`${time * member.id}`} style={{ width: '300px', height: '440px', borderRadius: '0', borderWidth: '0' }}>
                        <div className="card-body">
                                <div className='overflow-hidden'>
                                    <img src={member.image} className="card-img-top card-img-zoom" alt={member.fullName} />
                                </div>
                                <h5 className="card-title pt-3"><b>{member.fullName}</b></h5>
                                <p className="card-text py-2">{member.designation}</p>
                                <div className="rightsidebtn">
                                    <div className='position-relative'>
                                       <div className='overflow-hidden readmoreCon'>
                                            <i className="rightside-menu fa-solid fa-arrow-right position-relative arrhover" style={{zIndex:'5'}}></i>
                                            <p className='overmoresss position-relative overhover'>
                                                <i className="fa-brands fa-facebook-f"></i>
                                                <i className="ps-3 fa-brands fa-twitter"></i>
                                                <i className="ps-3 fa-brands fa-instagram"></i>
                                            </p>
                                       </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                ))}
            </div>
        </div>
    );
}

export default TeamCard;