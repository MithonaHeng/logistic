import React from "react";
import "../../assets/css/feature.css";
import Image from "../../assets/image/feature.jpg";
const SubOurFeature = () => {
  return (
    <>
      {/* our features  */}
      <section className=" container-xxl my-5 pt-5">
        <div className="row">
          {/* text left side  */}
          <div className="col-lg-5 col-md-12  mx-lg-0">
            <h6 className=" text-uppercase text-info pb-2">our features</h6>
            <h1 className=" text-capitalize pb-5">
              we are trusted logistics company since 1990
            </h1>

            <article
              className="d-flex mb-4"
              data-aos="fade-up"
              data-aos-duration="1000"
            >
              <div className="icon pe-3">
                <i className="fa-solid fa-globe"></i>
              </div>
              <div className="title text-start">
                <h5 className=" text-black text-capitalize">
                  worldwide service
                </h5>
                <p>
                  Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                  Odit, obcaecati!
                </p>
              </div>
            </article>
            <article
              className="d-flex mb-4"
              data-aos="fade-up"
              data-aos-duration="2000"
            >
              <div className="icon pe-3">
                <i className="fa-solid fa-truck-fast"></i>
              </div>
              <div className="title text-start">
                <h5 className=" text-black text-capitalize">
                  on time delivery
                </h5>
                <p>
                  Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                  Odit, obcaecati!
                </p>
              </div>
            </article>
            <article
              className="d-flex mb-4"
              data-aos="fade-up"
              data-aos-duration="2000"
            >
              <div className="icon pe-3">
                <i className="fa-solid fa-headphones"></i>
              </div>
              <div className="title text-start">
                <h5
                  className=" text-black text-capitalize"
                  data-aos="fade-zoom-in"
                  data-aos-easing="ease-in-back"
                  data-aos-delay="400"
                  data-aos-offset="0"
                >
                  24/7 telephone support
                </h5>
                <p>
                  Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                  Odit, obcaecati!
                </p>
              </div>
            </article>
          </div>
          {/* images  */}
          <div className="col-lg-7 col-md-12 mx-lg-0">
            <div
              className="image position-relative h-75"
              data-aos="fade-left"
              data-aos-anchor="#example-anchor"
              data-aos-offset="500"
              data-aos-duration="1000"
            >
              <img
                src={Image}
                alt="feature"
                style={{ objectFit: "cover" }}
                className=" w-100 h-100 img-fluid"
              />
            </div>
          </div>
        </div>
      </section>
    </>
  );
};

export default SubOurFeature;
