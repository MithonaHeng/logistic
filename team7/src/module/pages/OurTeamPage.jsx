import React from 'react';
import TeamCard from '../components/TeamCard';
import teamImage1 from '../../assets/image/team-1.jpg';
import teamImage2 from '../../assets/image/team-2.jpg';
import teamImage3 from '../../assets/image/team-3.jpg';
import teamImage4 from '../../assets/image/team-4.jpg';
import SmallBanner from '../components/SmallBanner';

const OurTeamPage = () => {
  const teamMembers = [
    { id: '1', fullName: 'Full Name', designation: 'Designation', image: teamImage1 },
    { id: '2', fullName: 'Full Name', designation: 'Designation', image: teamImage2 },
    { id: '3', fullName: 'Full Name', designation: 'Designation', image: teamImage3 },
    { id: '4', fullName: 'Full Name', designation: 'Designation', image: teamImage4 },
  ];

  return (
    <>
    <SmallBanner/>
    <div style={{backgroundColor: 'rgb(244,244,244)'}}>
      <TeamCard teamMembers={teamMembers} />
    </div>
    </>
  );
};

export default OurTeamPage;