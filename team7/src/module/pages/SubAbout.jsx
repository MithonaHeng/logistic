import React from "react";
import Img from "../../assets/image/about.jpg";
import "../../assets/css/about.css";
import "../../assets/css/feature.css";
function SubAbout() {
  return (
    <>
      <section className=" container-xxl my-5">
        <div className="row">
          <div className="col-lg-6">
            {/* image left side  */}
            <div
              className="image position-relative h-100"
              data-aos="fade-right"
              data-aos-anchor="#example-anchor"
              data-aos-offset="500"
              data-aos-duration="1000"
            >
              <img
                src={Img}
                alt="feature"
                style={{ objectFit: "cover" }}
                className=" w-100 h-100 img-fluid position-absolute"
              />
            </div>
          </div>
          <div className="col-lg-6" data-aos="fade-up" data-aos-duration="1000">
            {/* text right side  */}
            <h6 className=" text-uppercase text-info pb-2">about us</h6>
            <h1 className=" text-capitalize pb-5">
              quick transport and logistics solutions
            </h1>
            <p className=" pb-3">
              Lorem ipsum dolor sit amet consectetur adipisicing elit. Fugit
              nisi veritatis vitae quas distinctio fugiat ipsa est eum maxime
              voluptatem impedit, quibusdam, minima quae explicabo quod ipsam
              illum et inventore!
            </p>
            <div className="items d-flex">
              <article className=" px-2">
                <div className="icon mb-3">
                  <i className="fa-solid fa-globe "></i>
                </div>
                <div className="title text-start  mb-3">
                  <h5 className=" text-black text-capitalize">
                    worldwide service
                  </h5>
                  <p>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                    Odit.
                  </p>
                </div>
              </article>
              <article className="px-2">
                <div className="icon mb-3">
                  <i className="fa-solid fa-globe"></i>
                </div>
                <div className="title text-start  mb-3">
                  <h5 className=" text-black text-capitalize">
                    worldwide service
                  </h5>
                  <p>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                    Odit.
                  </p>
                </div>
              </article>
            </div>
            <button className="py-3 px-5 text-capitalize border-0 mt-3">
              explore more
            </button>
          </div>
        </div>
      </section>
    </>
  );
}

export default SubAbout;
