import React from "react";
import Phone from "../components/Phone";
import CountUp from "react-countup";
import "../../assets/css/feature.css";
const SubFeature = () => {
  return (
    <>
      {/* some facts */}
      <section className=" container-xxl my-5">
        <div className="row">
          <div className="col-lg-6" data-aos="fade-up" data-aos-duration="1000">
            {/* text left side */}
            <h6 className=" text-uppercase text-info pb-2">some facts</h6>
            <h1 className=" text-capitalize pb-5">
              #1 place to manage all of your shipments
            </h1>
            <p className=" pb-3">
              Lorem ipsum dolor sit amet consectetur adipisicing elit. Fugit
              nisi veritatis vitae quas distinctio fugiat ipsa est eum maxime
              voluptatem impedit, quibusdam, minima quae explicabo quod ipsam
              illum et inventore!
            </p>
            <Phone />
          </div>
          <div className="col-lg-6">
            <div className="row">
              <div className="col-lg-6 col-sm-6 p-0">
                <article
                  className="review d-flex flex-column m-3 text-start p-4"
                  id="review1"
                  data-aos="fade-zoom-in"
                  data-aos-easing="ease-in-back"
                  data-aos-delay="300"
                  data-aos-offset="0"
                >
                  <i className="fa-solid fa-users mb-2"></i>
                  <h2 className="text-white mb-2">
                    <CountUp start={300} end={1234} duration={3} />
                  </h2>
                  <p className=" text-capitalize">Happy clients</p>
                </article>

                <article
                  className="review d-flex flex-column m-3 text-start p-4"
                  id="review2"
                  data-aos="fade-zoom-in"
                  data-aos-easing="ease-in-back"
                  data-aos-delay="400"
                  data-aos-offset="0"
                >
                  <i className="fa-solid fa-ship mb-2"></i>
                  <h2 className="text-white mb-2">
                    <CountUp start={300} end={1234} duration={3} />
                  </h2>
                  <p className=" text-capitalize">Completed shipments</p>
                </article>
              </div>
              <div className="col-lg-6 col-sm-6 align-self-center p-0">
                <article
                  className="review d-flex mx-3 flex-column text-start p-4"
                  id="review3"
                  data-aos="fade-zoom-in"
                  data-aos-easing="ease-in-back"
                  data-aos-delay="400"
                  data-aos-offset="0"
                >
                  <i className="fa-solid fa-star mb-5"></i>
                  <h2 className="text-white mb-2">
                    <CountUp start={300} end={1234} duration={3} />
                  </h2>
                  <p className=" text-capitalize">Customers review</p>
                </article>
              </div>
            </div>
          </div>
        </div>
      </section>
    </>
  );
};

export default SubFeature;
