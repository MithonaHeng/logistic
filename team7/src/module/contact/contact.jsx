import React from 'react'
import ContactUs from '../components/ContactUs'
import SmallBanner from '../components/SmallBanner'

const contact = () => {
  return (<>
  <SmallBanner/>
  <section className=' container-xxl my-5'>
    <div className="row">
          {/* Form Contact  */}
      <div className="col-lg-6 col-md-12" data-aos="fade-zoom-in"
     data-aos-easing="ease-in-back"
     data-aos-delay="200"
     data-aos-offset="0">
      <h6 className=' text-uppercase text-info pb-2'>get in touch</h6>
          <h1 className=' text-capitalize pb-3'>contact for any query</h1>
          <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Deserunt quaerat, ducimus ea beatae reprehenderit ratione deleniti quo odio quia consequuntur.</p>
          <div className=" w-100 h-100 d-flex justify-content-center">
          <ContactUs/>
          </div>
      </div>
          {/* map  */}
      <div  data-aos="fade-left"
     data-aos-anchor="#example-anchor"
     data-aos-offset="500"
     data-aos-duration="1000" className="col-lg-6 d-flex align-items-center justify-content-center position-relative">
      <iframe className=' h-60 w-100' src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3908.906938714209!2d104.88630747584448!3d11.5585288442795!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x310951a6ff15ad8d%3A0x93fc16318df94a71!2sHeng%20Ly%20Market!5e0!3m2!1sen!2skh!4v1705636896800!5m2!1sen!2skh" style={{width:'900px', height:'500px' }}  loading="lazy"></iframe>
      </div>
    </div>
  </section>
  </>)
}

export default contact