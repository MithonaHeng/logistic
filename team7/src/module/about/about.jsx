import React from "react";
import Img from "../../assets/image/about.jpg";
import "../../assets/css/about.css";
import Image from "../../assets/image/feature.jpg";
import "../../assets/css/feature.css";
import Phone from "../components/Phone";
import CountUp from "react-countup";
import SmallBanner from "../components/SmallBanner";
import OurTeam from "../pages/OurTeam";

const About = () => {
  return (
    <>
      <SmallBanner />
      <section className=" container-xxl my-5">
        <div className="row">
          <div className="col-lg-6">
            {/* image left side  */}
            <div
              className="image position-relative h-100"
              data-aos="fade-right"
              data-aos-anchor="#example-anchor"
              data-aos-offset="500"
              data-aos-duration="1000"
            >
              <img
                src={Img}
                alt="feature"
                style={{ objectFit: "cover" }}
                className=" w-100 h-100 img-fluid position-absolute"
              />
            </div>
          </div>
          <div className="col-lg-6" data-aos="fade-up" data-aos-duration="1000">
            {/* text right side  */}
            <h6 className=" text-uppercase text-info pb-2">about us</h6>
            <h1 className=" text-capitalize pb-5">
              quick transport and logistics solutions
            </h1>
            <p className=" pb-3">
              Lorem ipsum dolor sit amet consectetur adipisicing elit. Fugit
              nisi veritatis vitae quas distinctio fugiat ipsa est eum maxime
              voluptatem impedit, quibusdam, minima quae explicabo quod ipsam
              illum et inventore!
            </p>
            <div className="items d-flex">
              <article className=" px-2">
                <div className="icon mb-3">
                  <i className="fa-solid fa-globe "></i>
                </div>
                <div className="title text-start  mb-3">
                  <h5 className=" text-black text-capitalize">
                    worldwide service
                  </h5>
                  <p>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                    Odit.
                  </p>
                </div>
              </article>
              <article className="px-2">
                <div className="icon mb-3">
                  <i className="fa-solid fa-globe"></i>
                </div>
                <div className="title text-start  mb-3">
                  <h5 className=" text-black text-capitalize">
                    worldwide service
                  </h5>
                  <p>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                    Odit.
                  </p>
                </div>
              </article>
            </div>
            <button className="py-3 px-5 text-capitalize border-0 mt-3">
              explore more
            </button>
          </div>
        </div>
      </section>
      <section className=" container-xxl my-5">
        <div className="row">
          <div className="col-lg-6">
            {/* image left side  */}
            <div
              className="image position-relative h-100"
              data-aos="fade-right"
              data-aos-anchor="#example-anchor"
              data-aos-offset="500"
              data-aos-duration="1000"
            >
              <img
                src={Img}
                alt="feature"
                style={{ objectFit: "cover" }}
                className=" w-100 h-100 img-fluid position-absolute"
              />
            </div>
          </div>
        </div>
      </section>

      <section className=" container-xxl my-5">
        <div className="row">
          <div className="col-lg-6" data-aos="fade-up" data-aos-duration="1000">
            {/* text left side */}
            <h6 className=" text-uppercase text-info pb-2">some facts</h6>
            <h1 className=" text-capitalize pb-5">
              #1 place to manage all of your shipments
            </h1>
            <p className=" pb-3">
              Lorem ipsum dolor sit amet consectetur adipisicing elit. Fugit
              nisi veritatis vitae quas distinctio fugiat ipsa est eum maxime
              voluptatem impedit, quibusdam, minima quae explicabo quod ipsam
              illum et inventore!
            </p>
            <Phone />
          </div>
          <div className="col-lg-6">
            <div className="row">
              <div className="col-lg-6 col-sm-6 p-0">
                <article
                  className="review d-flex flex-column m-3 text-start p-4"
                  id="review1"
                  data-aos="fade-zoom-in"
                  data-aos-easing="ease-in-back"
                  data-aos-delay="300"
                  data-aos-offset="0"
                >
                  <i className="fa-solid fa-users mb-2"></i>
                  <h2 className="text-white mb-2">
                    <CountUp start={300} end={1234} duration={3} />
                  </h2>
                  <p className=" text-capitalize">Happy clients</p>
                </article>

                <article
                  className="review d-flex flex-column m-3 text-start p-4"
                  id="review2"
                  data-aos="fade-zoom-in"
                  data-aos-easing="ease-in-back"
                  data-aos-delay="400"
                  data-aos-offset="0"
                >
                  <i className="fa-solid fa-ship mb-2"></i>
                  <h2 className="text-white mb-2">
                    <CountUp start={300} end={1234} duration={3} />
                  </h2>
                  <p className=" text-capitalize">Completed shipments</p>
                </article>
              </div>
              <div className="col-lg-6 col-sm-6 align-self-center p-0">
                <article
                  className="review d-flex mx-3 flex-column text-start p-4"
                  id="review3"
                  data-aos="fade-zoom-in"
                  data-aos-easing="ease-in-back"
                  data-aos-delay="400"
                  data-aos-offset="0"
                >
                  <i className="fa-solid fa-star mb-5"></i>
                  <h2 className="text-white mb-2">
                    <CountUp start={300} end={1234} duration={3} />
                  </h2>
                  <p className=" text-capitalize">Customers review</p>
                </article>
              </div>
            </div>
          </div>
        </div>
      </section>
      {/* our features  */}
      <section className=" container-xxl my-5 pt-5">
        <div className="row">
          {/* text left side  */}
          <div className="col-lg-5 col-md-12  mx-lg-0">
            <h6 className=" text-uppercase text-info pb-2">our features</h6>
            <h1 className=" text-capitalize pb-5">
              we are trusted logistics company since 1990
            </h1>

            <article
              className="d-flex mb-4"
              data-aos="fade-up"
              data-aos-duration="1000"
            >
              <div className="icon pe-3">
                <i className="fa-solid fa-globe"></i>
              </div>
              <div className="title text-start">
                <h5 className=" text-black text-capitalize">
                  worldwide service
                </h5>
                <p>
                  Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                  Odit, obcaecati!
                </p>
              </div>
            </article>
            <article
              className="d-flex mb-4"
              data-aos="fade-up"
              data-aos-duration="2000"
            >
              <div className="icon pe-3">
                <i className="fa-solid fa-truck-fast"></i>
              </div>
              <div className="title text-start">
                <h5 className=" text-black text-capitalize">
                  on time delivery
                </h5>
                <p>
                  Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                  Odit, obcaecati!
                </p>
              </div>
            </article>
            <article
              className="d-flex mb-4"
              data-aos="fade-up"
              data-aos-duration="2000"
            >
              <div className="icon pe-3">
                <i className="fa-solid fa-headphones"></i>
              </div>
              <div className="title text-start">
                <h5
                  className=" text-black text-capitalize"
                  data-aos="fade-zoom-in"
                  data-aos-easing="ease-in-back"
                  data-aos-delay="400"
                  data-aos-offset="0"
                >
                  24/7 telephone support
                </h5>
                <p>
                  Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                  Odit, obcaecati!
                </p>
              </div>
            </article>
          </div>
          {/* images  */}
          <div className="col-lg-7 col-md-12 mx-lg-0">
            <div
              className="image position-relative h-75"
              data-aos="fade-left"
              data-aos-anchor="#example-anchor"
              data-aos-offset="500"
              data-aos-duration="1000"
            >
              <img
                src={Image}
                alt="feature"
                style={{ objectFit: "cover" }}
                className=" w-100 h-100 img-fluid"
              />
            </div>
          </div>
        </div>
      </section>

      <OurTeam />
    </>
  );
};

export default About;
