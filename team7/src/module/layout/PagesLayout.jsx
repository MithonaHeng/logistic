import React from 'react'
import {Outlet } from 'react-router-dom'
const PagesLayout = () => {
  return (
    <div>
        <Outlet/>
    </div>
  )
}

export default PagesLayout