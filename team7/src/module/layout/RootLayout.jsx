import React from 'react'
import { Outlet } from 'react-router-dom'
import NavBar from './header/NavBar'
import Footer from './footer/Footer'
const RootLayout = () => {
  return (
    <div>
        <NavBar/>
        <Outlet/>
        <Footer/>
    </div>
  )
}

export default RootLayout