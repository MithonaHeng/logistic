import React from "react";
import "./Footer.css";

const Footer = () => {
  return (
    <div>
      <div className=" container-fluid footer bg-dark text-light pt-5 mt-5">
        <div className=" container py-5">
          <div className=" row">
            <div className="col-lg-3 col-md-6 mb-2">
              <h4 className=" text-light mb-4">Address</h4>
              <p className=" mb-2">
                <i className="fa-solid fa-location-dot text-light me-3"></i>
                123 Street, New York,USA
              </p>
              <p className=" mb-2">
                <i className="fa-solid fa-phone text-light me-3"></i>
                +012 345 67890
              </p>
              <p className=" mb-2">
                <i className="fa-solid fa-envelope text-light"></i>
                kiloIT@gmail.com
              </p>
              <div className=" d-flex pt-2">
                <a href="#" className="btn-socail">
                  <i className="fa-brands fa-twitter"></i>
                </a>
                <a href="#" className="btn-socail">
                  <i className="fa-brands fa-facebook"></i>
                </a>
                <a href="#" className="btn-socail">
                  <i className="fa-brands fa-youtube"></i>
                </a>
                <a href="#" className="btn-socail">
                  <i className="fa-brands fa-linkedin"></i>
                </a>
              </div>
            </div>
            <div className="col-lg-3 col-md-6">
              <h4 className=" text-light mb-4">Service</h4>
              <a href="#" className="btn-links">
                <i className="fa-solid fa-chevron-right me-2"></i> Air Freight
              </a>
              <a href="#" className="btn-links">
                <i className="fa-solid fa-chevron-right me-2"></i> Sea Freight
              </a>
              <a href="#" className="btn-links">
                <i className="fa-solid fa-chevron-right me-2"></i> Road Freight
              </a>
              <a href="#" className="btn-links">
                <i className="fa-solid fa-chevron-right me-2"></i> Logistic
                Solutions
              </a>
              <a href="#" className="btn-links">
                <i className="fa-solid fa-chevron-right me-2"></i> Industry
                Solutions
              </a>
            </div>
            <div className="col-lg-3 col-md-6">
              <h4 className=" text-light mb-4">Quick Links</h4>
              <a href="#" className="btn-links">
                <i className="fa-solid fa-chevron-right me-2"></i> About Us
              </a>
              <a href="#" className="btn-links">
                <i className="fa-solid fa-chevron-right me-2"></i> Contact Us
              </a>
              <a href="#" className="btn-links">
                <i className="fa-solid fa-chevron-right me-2"></i> Our Service
              </a>
              <a href="#" className="btn-links">
                <i className="fa-solid fa-chevron-right me-2"></i> Terms &
                Condition
              </a>
              <a href="#" className="btn-links">
                <i className="fa-solid fa-chevron-right me-2"></i> Support
              </a>
            </div>
            <div className="col-lg-3 col-md-6">
              <h4 className=" text-light mb-4">Newsletter</h4>
              <p>Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet.</p>
              <div className="mb-3 d-flex position-relative">
                <input
                  type="email"
                  className="form-control w-100 p-3"
                  id="exampleFormControlInput2"
                  placeholder="email"
                ></input>
                <button
                  type="button"
                  className="btn btn-danger  position-absolute btn-signin m-2 px-2"
                >
                  Sign-in
                </button>
              </div>
            </div>
          </div>
        </div>
        <div className="container">
          <div className="copyright">
            <div className="row">
              <div className="col-md-6 text-center text-md-start mt-2">
                <p className=" text-light">
                  <i className="fa-regular fa-copyright"></i>
                  <a href="#" className=" text-danger fw-bold"> Logistica</a>, All Rigth Reserved.
                </p>
              </div>
              <div className="col-md-6 text-center text-md-end mt-2">
                Designer By <a href="#" className=" text-danger fw-bold">HTML CODE</a><br />
                Distributed <a href="#" className=" text-light fw-bold">By ThemeWagon</a>
              </div>
              
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Footer;
