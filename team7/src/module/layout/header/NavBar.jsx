import React from 'react';
import { Link, useLocation } from 'react-router-dom';
import { Navbar, Nav, NavDropdown } from 'react-bootstrap';

const NavBar = () => {
  const location = useLocation();

  const isActive = (path) => {
    return location.pathname === path ? "nav-link text-danger active" : "nav-link text-dark";
  };
  return (
    <Navbar expand="lg" variant="light" bg="light" className="sticky-top top-0 shadow border-5  border-top border-danger p-0">
      <Navbar.Brand className="d-flex text-700 px-4 py-3 " style={{ backgroundColor: '#FF3E41', color: '#FFFFFF' }}>
      <h2 className='text-light px-3'>Logistica</h2>
      </Navbar.Brand>
      <Navbar.Toggle aria-controls="basic-navbar-nav" />
      <Navbar.Collapse id="basic-navbar-nav">
        <Nav className="justify-content-end w-100 text-uppercase mx-3 gap-3 fw-medium">
          <Nav.Link as={Link} to="/" className={isActive("/")} >Home</Nav.Link>
          <Nav.Link as={Link} to="/about" className={isActive("/about")}>About</Nav.Link>
          <Nav.Link as={Link} to="/service" className={isActive("/service")}>Services</Nav.Link>
          <NavDropdown title="Pages" id="basic-nav-dropdown" className="p-0">
            <NavDropdown.Item as={Link} to="/pricing" >Pricing plan</NavDropdown.Item>
            <NavDropdown.Item as={Link} to="/feature">Features</NavDropdown.Item>
            <NavDropdown.Item as={Link} to="/freequote">Free quote</NavDropdown.Item>
            <NavDropdown.Item as={Link} to="/ourteam">Our team</NavDropdown.Item>
            <NavDropdown.Item as={Link} to="/testimonail">Testimonial</NavDropdown.Item>
            <NavDropdown.Item as={Link} to="/error">Error</NavDropdown.Item>
          </NavDropdown>
          <Nav.Link as={Link} to="/contact" className={isActive("/contact")}>Contact</Nav.Link>
          <div className='d-flex align-items-center m-0 px-2'>
            <h4 className='me-4'>
              <i className="fa fa-headphones text-danger me-3"></i>
              <span className='fs-4'>+012 345 6789</span>
            </h4>
          </div>
        </Nav>
      </Navbar.Collapse>
    </Navbar>
  );
}

export default NavBar;
