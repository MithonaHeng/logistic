import Silder from "../components/Silder";
import OurTeam from "../pages/OurTeam";
import SubAbout from "../pages/SubAbout";
import SubFeature from "../pages/SubFeature";
import SubOurFeature from "../pages/SubOurFeature";
import SubPricing from "../pages/SubPricing";
import SubServicePage from "../pages/SubServicePage";
import SubTestimonail from "../pages/SubTestimonail";

const Home = () => {
  return (
    <div>
      <Silder />
      <SubAbout />
      <SubFeature />
      <SubServicePage />
      <SubOurFeature />
      <SubPricing />
      <OurTeam />
      <SubTestimonail />
    </div>
  );
};

export default Home;
