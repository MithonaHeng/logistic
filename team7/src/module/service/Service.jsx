
import React from 'react';
import Testimonail from'../components/Testimonail'
import ServiceCard from '../components/ServiceCard';
import service1 from '../../assets/image/service-1.jpg';
import service2 from '../../assets/image/service-2.jpg';
import service3 from '../../assets/image/service-3.jpg';
import service4 from '../../assets/image/service-4.jpg';
import service5 from '../../assets/image/service-5.jpg';
import service6 from '../../assets/image/service-6.jpg';
import SmallBanner from '../components/SmallBanner';


const ServicePage = () => {
  const serviceInfo = [
    { id: '1', fullName: 'Air Freight', designation: 'Stet stet justo dolor sed duo. Ut clita sea sit ipsum diam lorem diam.', image: service1 },
    { id: '2', fullName: 'Ocean Freight ', designation: 'Stet stet justo dolor sed duo. Ut clita sea sit ipsum diam lorem diam.', image: service2 },
    { id: '3', fullName: 'Road Freight', designation: 'Stet stet justo dolor sed duo. Ut clita sea sit ipsum diam lorem diam.', image: service3 },
    { id: '4', fullName: 'Train Freight', designation: 'Stet stet justo dolor sed duo. Ut clita sea sit ipsum diam lorem diam.', image: service4 },
    { id: '5', fullName: 'Customs Clearance', designation: 'Stet stet justo dolor sed duo. Ut clita sea sit ipsum diam lorem diam.', image: service5 },
    { id: '6', fullName: 'Warehouse Solutions', designation: 'Stet stet justo dolor sed duo. Ut clita sea sit ipsum diam lorem diam.', image: service6 },
  ];

  return (
    <>
    <SmallBanner/>
    <div style={{backgroundColor: 'rgb(244,244,244)'}}>
      <ServiceCard serviceInfo={serviceInfo} />
      <Testimonail/>
    </div>
    </>
  );
};

export default ServicePage;
